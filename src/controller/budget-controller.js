import { Router } from "express";
import { BudgetRepository } from "../repository/budgetRepo";

export const budgetController = Router();

budgetController.get('/', async (req, res) => {
    try {
        let data;
        if (req.query.search) {
            data = await BudgetRepository.search(req.query.search);
        } else {
            data = await BudgetRepository.findAll();
        }
        res.json(data);
     } catch (error) {
            console.log(error);
            res.status(500).end();
        }
    })

budgetController.get('/:id', async (req, res) => {
    try {
        let data = await BudgetRepository.findById(req.params.id);
        res.json(data);

    } catch (error) {
        console.log(error);
        res.status(204).end;
    }
})

budgetController.patch('/:id', async (req, res) => {
    try {
        let data = await BudgetRepository.findById(req.params.id);
        if (!data) {
            res.status(404).end();
            return
        }
        let update = { ...data, ...req.body };
        await BudgetRepository.updateBudget(update);
        res.json(update);

    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})


budgetController.get('/month/:month', async (req, res) => {
    try {
        let data = await BudgetRepository.findByDate(req.params.month)
        res.json(data);
        res.end();

    } catch (error) {
        console.log(error);
        res.status(404).end()
    }
})


budgetController.post('/', async (req, res) => {
    try {

        await BudgetRepository.addBudget(req.body);
        res.json(req.body);
    } catch (error) {
        console.log(error);
        res.status(404).end();
    }
})

budgetController.put('/', async (req, res) => {
    try {
        await BudgetRepository.updateBudget(req.body);
        res.json();
    } catch (error) {
        console.log(error);
        res.status(200).end();
    }
})

budgetController.delete('/:id', async (req, res) => {
    try {
        await BudgetRepository.deleteBudget(req.params.id);
        res.status(204).end();
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

