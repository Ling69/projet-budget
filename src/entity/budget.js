
export class Budget {
    id;
    category;
    title;
    price;
    date;

    /**
     * 
     * @param {String} category 
     * @param {String} title 
     * @param {Number} price 
     * @param {String} date 
     * @param {Number} id 
     */
    constructor(category, title, price, date, id) {
        this.id = id;
        this.category = category;
        this.title = title;
        this.price = price;
        this.date = date;
    }

}