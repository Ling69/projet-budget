import { Budget } from '../entity/budget';
import { connection } from './bddConnector';


export class BudgetRepository {

    /**
     * get all budgets from database
     * @returns {Promise<Budget[]>};
     */
    static async findAll() {

        const[rows] = await connection.execute('SELECT * FROM budget');
        const budgets = [];
        for (const row of rows) {
            const budget = new Budget(row.category, row.title, row.price, row.date, row.id);
            budgets.push(budget) 
        }
        return budgets;

    }

    /**
     * search budgets in db
     * @param {Any} search search one of budgets
     * @returns Budgets which category, title and date match the search
     */
    static async search(search) {

        const [rows] = await connection.query('SELECT * FROM budget WHERE CONCAT(category,title,date) LIKE ?', ['%'+search+'%'])
        
        return rows.map(row => new Budget(row['category'], row['title'], row['date'], Boolean(row['available']), row['id']));
       
    }

    /**
     * function which find one Budget by Id in db
     * @param {Number} id 
     * @returns {Promise<Budget}
     */
    static async findById (id) {
        let [rows] = await connection.execute('SELECT * FROM budget WHERE id=?', [id]);
        if (rows.length === 0) {
            return null;
        } else {
            return new Budget(rows[0].category, rows[0].title, rows[0].price, rows[0].date);
        }
        
    }

    /**
    * 
    * @param {Number} month 
    * @returns {Promise<Budget[]>}
    */
    static async findByDate(month){
        const [rows] = await connection.execute('SELECT * FROM budget WHERE MONTH (date)=?', [month]);
        const budgetData = [];
        for (const row of rows) {
            let instance = new Budget(row.category, row.title, row.price, row.date, row.id);
            budgetData.push(instance);
        }
        return budgetData;
    }

    /**
    * Fonction qui ajouter un Budget en BDD
    * @param {Budget} budgetData 
    */
    static async addBudget (budgetData) {
        const [rows] = await connection.execute('INSERT INTO budget (category, title, price, date) VALUES (?, ? , ?, ?)', [budgetData.category, budgetData.title, budgetData.price, budgetData.date]);
        budgetData.id = rows.insertId;
    }

    /**
     * Fonction qui update un Budget en BDD
     * @param {Budget} updateData 
     */
    static async updateBudget(updateData) {
        await connection.execute('UPDATE budget SET category=?, title=?, price=?, date=? WHERE id=?', [updateData.category, updateData.title, updateData.price, updateData.date, updateData.id]);
    }

    /**
     * Fonction qui supprime un Budget en BDD
     * @param {Budget} id 
     */
    static async deleteBudget(id) {
        await connection.execute('DELETE FROM budget WHERE id=?', [id]);
    }

}

