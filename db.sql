DROP TABLE IF EXISTS `budget`;
CREATE TABLE `budget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

INSERT INTO budget(category,title,price,date) VALUES 
('restaurent', 'pizza hot', 10,'2021/05/08'),
('clothes', 'HM', 20, '2021/05/15'),
('entertainment', 'Museum Beau Art', 15, '2021/05/22'),
('others', 'Birthday gift', 30, '2021/05/29');

