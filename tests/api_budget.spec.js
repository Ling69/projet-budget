import request from "supertest";
//import { connection } from "../src/repository/bddConnector";
import { server } from "../src/server";

describe('API Budget routes', ()=> {

    it('should be on return budget list on GET ', async () =>{
        let response = await request(server)
        .get('/api/budget')
        .expect(200);

        expect(response.body).toContainEqual({
            id: expect.any(Number),
            category: expect.any(String),
            title: expect.any(String),
            price: expect.any(Number),
            date: expect.any(String)
        })

    })
})
